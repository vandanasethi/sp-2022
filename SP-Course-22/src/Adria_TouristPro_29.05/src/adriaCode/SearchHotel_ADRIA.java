package adriaCode;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException; 
import javax.mail.Transport;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.sun.mail.handlers.multipart_mixed;

public class SearchHotel_ADRIA extends JFrame {

	private JComboBox<Hotel> cb_hotel;
	private DefaultComboBoxModel<Hotel> model;
	private JTextField tf_hotelid;
	private JTextField tf_hotelcat;
	private JTextField tf_hotelname;
	private JTextField tf_hotelowner;
	private JTextField tf_contact;
	private JTextField tf_address;
	private JTextField tf_city;
	private JTextField tf_citycode;
	private JTextField tf_phone;
	private JTextField tf_nrofrooms;
	private JTextField tf_nrofbeds;
	//private JTextField tf_gender;
	//private JCheckBox cbtravelAlone;
	private JButton bt_editHotel;
	private JButton bt_addNewHotel;
	private JButton bt_showRelatedTransactions;
	private JButton bt_deleteHotel;
	private JButton bt_backupHotels;
	private JButton bt_sendEmail;
	
	
	private JLabel logo;
	private Icon adria;	
	
	private List<Hotel> hotels = (List<Hotel>) MasterData.HotelList();
	

	public SearchHotel_ADRIA() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
		logo = new JLabel(adria);
		
		model = new DefaultComboBoxModel<>();
		cb_hotel = new JComboBox<>(model);
		tf_hotelid = new JTextField(5);
		tf_hotelcat = new JTextField(5);
		tf_hotelname = new JTextField(20);
		tf_hotelowner = new JTextField(15);
		tf_contact = new JTextField(15);
		tf_address = new JTextField(20);
		tf_city = new JTextField(10);
		tf_citycode = new JTextField(10);
		tf_phone = new JTextField(10);
		tf_nrofrooms = new JTextField(5);
		tf_nrofbeds = new JTextField(5);
		//tf_gender = new JTextField(10);
		//cbtravelAlone = new JCheckBox("");
		bt_editHotel = new JButton("Edit Hotel");
		bt_addNewHotel = new JButton("Add A New Hotel");
		bt_showRelatedTransactions = new JButton("Show Related Transactions");
		bt_deleteHotel= new JButton("Delete This Hotel");
		bt_backupHotels = new JButton("Save Backup Hotels");
		bt_sendEmail = new JButton("Send E-Mail");
		setSize(950, 600);

		setLayout(new FlowLayout());
		setTitle("Hotel And Transaction Search");
		add(logo, BorderLayout.NORTH);
		add(new JLabel("ID:"));
		add(tf_hotelid);
		tf_hotelid.setEnabled(false);
		add(cb_hotel);
		add(new JLabel("Category:"));
		add(tf_hotelcat);
		add(new JLabel("Name:"));
		add(tf_hotelname);
		add(new JLabel("Owner:"));
		add(tf_hotelowner);
		add(new JLabel("Contact:"));
		add(tf_contact);
		add(new JLabel("Address:"));
		add(tf_address);
		add(new JLabel("City:"));
		add(tf_city);
		add(new JLabel("Citycode:"));
		add(tf_citycode);
		add(new JLabel("Phone:"));
		add(tf_phone);
		add(new JLabel("Number Of Rooms:"));
		add(tf_nrofrooms);
		add(new JLabel("Number Of Beds:"));
		add(tf_nrofbeds);
		//add(new JLabel("Gender:"));
		//add(tf_gender);
		//add(new JLabel("Travel alone:"));
		//add(cbtravelAlone);
		add(bt_editHotel);
		add(bt_addNewHotel);
		add(bt_showRelatedTransactions);
		add(bt_deleteHotel);
		add(bt_backupHotels);
		add(bt_sendEmail);

		bt_editHotel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Hotel h = (Hotel) cb_hotel.getSelectedItem();
				//h.setId(Integer.valueOf(tf_hotelid.getText()));

				String category = tf_hotelcat.getText();
				if (category.equals("*")) {
					h.setCategory(Category.ONE);
					
				} else if (category.equals("**")) {
					h.setCategory(Category.TWO);

				} else if (category.equals("***")) {
					h.setCategory(Category.THREE);

				} else if (category.equals("****")) {
					h.setCategory(Category.FOUR);
				
				} else if (category.equals("*****")) {
					h.setCategory(Category.FIVE);
				}
				
				h.setName(tf_hotelname.getText());
				
				h.setOwner(tf_hotelowner.getText());
				
				h.setContact(tf_contact.getText());
				
				h.setAddress(tf_address.getText());
				
				h.setCity(tf_city.getText());
				
				h.setCitycode(tf_citycode.getText());
				
				h.setPhone(tf_phone.getText());
				
				h.setNumRooms(Integer.valueOf(tf_nrofrooms.getText()));
				
				h.setNumBeds(Integer.valueOf(tf_nrofbeds.getText()));

				//String gender = tf_gender.getText();
				//if (gender.equals("MALE")) {
					//h.setGender(Gender.MALE);

				//} else if (gender.equals("FEMALE")) {
					//h.setGender(Gender.FEMALE);

				//} else if (gender.equals("INTER")) {
					//h.setGender(Gender.INTER);

				//}
				 				
				
				model.setSelectedItem(h);
				cb_hotel.repaint();

			}
		});
		
		cb_hotel.setModel(model);

		/*List<Hotel> hotels = new ArrayList<>();
		
		
		hotels.add(new Hotel(661,"*","Gasthof Singula","Strelitzer Daouia","Gschihay Mateeas","Hauptstrasse 22","Payerbach","2650","2853103",62,40));
		hotels.add(new Hotel(145,"*","Konditorei Massauer","Groebler Trinity","Goetzfried Himat","Boehmgasse 19","Waidhofen","3830","2882571",12,15));
		hotels.add(new Hotel(55,"***","Hotel garni Steinfeld","Niederhametner Kilian-Noel","Klopstech-Wieter Sakine","Nikolaus-August-Otto-Strasse 4","Wiener Neustadt","2700","3563381",79,100));
		hotels.add(new Hotel(353,"*","Land- und Seminarpension Gamerith","Tichauer Annamaeria","Mowinckel Luta","Mottingeramt 41","Mottingeramt","3532","2375369",54,131));
		hotels.add(new Hotel(332,"***","Haus der Pferde","Socjnoha Armella","Koriska Lilli-Valerie","Langschwarza 46","Schrems","3944","4938109",34,7));
		*/
		
//		Hotel h = TouristDAO.search(1);
//		model.addElement(h);

		for (Hotel currentHotel : hotels) {
			model.addElement(currentHotel);
		}

		cb_hotel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Hotel h = (Hotel) model.getSelectedItem();

				System.out.println(h);

				tf_hotelid.setText("" + h.getId());
				tf_hotelcat.setText("" + h.getCategory()); //PROBLEM!! (tf_hotelcat.setText(h.getCategory().toString());
				tf_hotelname.setText(h.getName());
				tf_hotelowner.setText(h.getOwner());
				tf_contact.setText(h.getContact());
				tf_address.setText(h.getAddress());
				tf_city.setText(h.getCity());
				tf_citycode.setText(h.getCitycode());
				tf_phone.setText(h.getPhone());
				tf_nrofrooms.setText("" + h.getNumRooms());
				tf_nrofbeds.setText("" + h.getNumBeds());
				//tf_gender.setText(t.getGender().toString());
				//cbtravelAlone.setSelected(t.isTravelAlone());

			}
		});
		
		bt_addNewHotel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				Hotel h = new Hotel(999, null, "", "", "", "", "", "", "", 4, 6);
				model.addElement(h);
				cb_hotel.repaint();
				cb_hotel.getSelectedItem();
				h.setId(Integer.valueOf(tf_hotelid.getText()));

				String category = tf_hotelcat.getText();//tf_hotelcat.getText());
				if (category.equals("*")) {
					h.setCategory(Category.ONE);
					
				} else if (category.equals("**")) {
					h.setCategory(Category.TWO);

				} else if (category.equals("***")) {
					h.setCategory(Category.THREE);

				} else if (category.equals("****")) {
					h.setCategory(Category.FOUR);
				
				} else if (category.equals("*****")) {
					h.setCategory(Category.FIVE);
				}
				
				h.setName("");//tf_hotelname.getText());
				
				h.setOwner("");//tf_hotelowner.getText());
				
				h.setContact("");//tf_contact.getText());
				
				h.setAddress("");//tf_address.getText());
				
				h.setCity("");//tf_city.getText());
				
				h.setCitycode("");//tf_citycode.getText());
				
				h.setPhone("");//tf_phone.getText());
				
				h.setNumRooms(Integer.valueOf(0));//Integer.valueOf(tf_nrofrooms.getText()));
				
				h.setNumBeds(Integer.valueOf(0));//Integer.valueOf(tf_nrofbeds.getText()));

				//String gender = tf_gender.getText();
				//if (gender.equals("MALE")) {
					//h.setGender(Gender.MALE);

				//} else if (gender.equals("FEMALE")) {
					//h.setGender(Gender.FEMALE);

				//} else if (gender.equals("INTER")) {
					//h.setGender(Gender.INTER);

				//}
				//Hotel h = (Hotel) cb_hotel.add(
				//Hotel currentHotel = new Hotel(id, category, name, owner, contact, address, city, citycode, phone, numRooms, numBeds);
				//model.add(newHotel); 				
				
				model.setSelectedItem(h);
				cb_hotel.repaint();

			}
		});
		
		bt_showRelatedTransactions.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Hotel h = (Hotel) cb_hotel.getSelectedItem();
				//h.setId(Integer.valueOf(tf_hotelid.getText()));
				new ShowTransactions_ADRIA().setVisible(true);
				//AddTransDat_ADRIA.cb_hotel.setCb_hotel(h) ---- Auf AddTransDat w�re eine JCB zu installieren
			}
		});
		
		bt_backupHotels.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				JFileChooser jfc = new JFileChooser();
//				int answer = jfc.showSaveDialog(null);
//				System.out.println("checkpoint: " + answer);
//				String path = null;
//
//				if (answer == JFileChooser.APPROVE_OPTION) {
//					path = jfc.getSelectedFile().getAbsolutePath();
//					System.out.println(path);
//				}

				String timestamp = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
				//File f1 = new File(path + timestamp);
				File f1 = new File("src/adriaCode/BackupMasterDat" + timestamp);

				try (BufferedWriter writer = new BufferedWriter(new FileWriter(f1))) {
					for (Hotel hotel : hotels) { //Hotel.getALL_HOTELS()
		
						writer.write(hotel.hotelToCSV() + "\n");
					}
		
				} catch (Exception exc) {
					System.err.println(exc.getLocalizedMessage());
				}
			}

		});
		
		
		bt_sendEmail.addActionListener(new ActionListener() {
 
			@Override
			public void actionPerformed(ActionEvent e) {
				
				ArrayList<String> to = new ArrayList<String>();
				to.add("timo_beil@gmx.at");
//				to.add(fhb201476);
//				String to = "timo_beil@gmx.at";
				
				String user = "AdriaTourismGmbH@gmx.net";
				//String user = "emailversenden@gmx.at";
				String pwd = "PiazziMa"; 
				//String pwd = "emailversenden";
				
				String host = "mail.gmx.net";
				//String text = "Did you get this test email?";
								
				Properties properties = System.getProperties();
				
				properties.put("mail.smtp.auth", "true");
				properties.put("mail.smtp.starttls.enable", "true");
				properties.put("mail.smtp.host", host);
				properties.put("mail.smtp.port", "587");
				
				Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
					
					protected PasswordAuthentication getPasswordAuthentication() {
						
						return new PasswordAuthentication(user, pwd);
					}
				});
				
				try {
					MimeMessage message = new MimeMessage(session);
					
					message.setFrom(new InternetAddress(user));
//					message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
					for (int i = 0; i <to.size(); i++) {
						message.addRecipient(Message.RecipientType.TO, new InternetAddress(to.get(i)));
					}
							
					
					String timestamp = new SimpleDateFormat("yyyyMMdd'.txt'").format(new Date());
					message.setSubject("Master Data " + timestamp);
					//message.setText(text);
					
					MimeMultipart multipart = new MimeMultipart();
					MimeBodyPart attachmentPart = new MimeBodyPart();
					MimeBodyPart textPart = new MimeBodyPart();
					String text = "Dear Mr. Piazzi, /n this is the current Master-Data version.";
					textPart.setContent(text, "UTF-8");
					
					File f2 = new File("src/E-Mail_" + timestamp);

					try (BufferedWriter writer = new BufferedWriter(new FileWriter(f2))) {
						for (Hotel hotel : hotels) { //Hotel.getALL_HOTELS()
			
							writer.write(hotel.hotelToCSV() + "\n");
						}
			
					} catch (Exception exc) {
						System.err.println(exc.getLocalizedMessage());
					}
					attachmentPart.attachFile(f2);
					multipart.addBodyPart(attachmentPart);
					//multipart.addBodyPart(textPart); = funktioniert nicht, warum???
					message.setContent(multipart);
					
					Transport.send(message);
					
				} catch (MessagingException ex) {
					ex.printStackTrace();
				} catch (IOException ioex) {
					ioex.printStackTrace();
				}
				
			}

		});
	}


//	public static ArrayList<Hotel> getHotels() {
//		ArrayList<Hotel>hotels = hotels;
//		return hotels;
//	}

}
