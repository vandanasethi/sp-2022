package adriaCode;

public enum Category {
	ONE("*"), TWO("**"), THREE("***"), FOUR("****"), FIVE("*****");
	
	private String abbr;
	private Category(String abbr) {
		this.abbr = abbr;
	}
	
	@Override
	public String toString() {
		return this.abbr;
	}
}
