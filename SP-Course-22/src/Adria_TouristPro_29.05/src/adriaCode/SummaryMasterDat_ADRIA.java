package adriaCode;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class SummaryMasterDat_ADRIA extends JFrame {

	JPanel top_panel;
	private JLabel logo;
	private Icon adria;	
	
	//ArrayList<Hotel> hotels = (ArrayList<Hotel>) MasterData.HotelList(); Brauch ma ned, hol ma direkt vo da MastaData
	ArrayList<HotelPerCat> hotelsPerCategory = new ArrayList<HotelPerCat>();

	public SummaryMasterDat_ADRIA() {

		setSize(950, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
		logo = new JLabel(adria);
//		top_panel = new JPanel();
//		top_panel.setPreferredSize(new Dimension(400, 400));
//		top_panel.setBackground(Color.green);
//		top_panel.add(new JLabel("out of the top panel area"));
//
//		add(top_panel, BorderLayout.NORTH);
//		add(new JLabel("HI"), BorderLayout.SOUTH);
		add(logo, BorderLayout.NORTH);

		String[] columns = {"Categories", "Establishments", "Rooms", "Beds"};		
		DefaultTableModel model = new DefaultTableModel();
		
		hotelsPerCategory.add(new HotelPerCat(Category.ONE));/// {ONE, TWO, THREE, FOUR, FIVE}; Array hat noch keinen Inhalt. M�ssen aber nur 
		hotelsPerCategory.add(new HotelPerCat(Category.TWO));
		hotelsPerCategory.add(new HotelPerCat(Category.THREE));
		hotelsPerCategory.add(new HotelPerCat(Category.FOUR));
		hotelsPerCategory.add(new HotelPerCat(Category.FIVE));
		//hotelsPerCategory.setSize(5);
		for (HotelPerCat hotel : hotelsPerCategory) {
			CountCategories(MasterData.HotelList(), hotel);
		}
		
		for (String column : columns) {
			//System.out.println("C");
			model.addColumn(column);
		}
		model.addRow(new Object[] {"Categories" , "Establishments", "Rooms", "Beds"});
		for (HotelPerCat hotelPerCategory : hotelsPerCategory) {
			model.addRow(new Object[] {hotelPerCategory.getCategory(), hotelPerCategory.getNoOfEstablishments(), hotelPerCategory.getNoOfRooms(), hotelPerCategory.getNoOfBeds()});
		}
//alle hotels
//		for (Hotel hotel : hotels) {
//			model.addRow(new Object[] {hotel.getId(), hotel.getCategory(), hotel.getName(), hotel.getOwner(), hotel.getContact(), hotel.getAddress(), hotel.getCity(), hotel.getCitycode(), hotel.getPhone(), hotel.getNumRooms(), hotel.getNumBeds()});
//		}
		JTable table = new JTable(model);
		add(table);
		

		setVisible(true);

	}
	
	public HotelPerCat CountCategories(List<Hotel> hotelInp, HotelPerCat categoryInp) {
				
		for(Hotel hotel : hotelInp) {
			if(hotel.getCategory() == categoryInp.getCategory()) {
				categoryInp.setNoOfEstablishments(categoryInp.getNoOfEstablishments() +1);
				categoryInp.setNoOfRooms(categoryInp.getNoOfRooms() + hotel.getNumRooms());
				categoryInp.setNoOfBeds(categoryInp.getNoOfBeds() + hotel.getNumBeds());
			}
		}
		return categoryInp;
	}
	
//	private Object[][] convertIntoArray(ArrayList<Studento> names) {
//
//		Object[][] students = new Object[names.size()][];
//		int pos = 0; 
//		
//		for (Studento currentStudent : names) {
//			students[pos] = new Object[] {currentStudent.getId(), currentStudent.getName(), currentStudent.getGrad()};
//			pos++;
//		}
//		
//		
//		return students;
//	}

}
