package adriaCode;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

public class MainWindow_SenUser_ADRIA extends JFrame implements ActionListener {

	/*Wie soll das Hauptmen� des SeniorUsers aussehen = Welche Buttons und Items ben�tigt er?
	 *Vorschlag:
	 *1) Button Suche Hotel => Neues Fenster �ffnet sich, mit JComboBox (UserStory #22). Dort werden neue
	 *			Buttons angelegt, u.a.: Neues Hotel hinzuf�gen (#3), eine neue Transaktion zu
	 *			einem bestehenden Hotel hinzuf�gen (#6), ein bestehendes Hotel l�schen (#11)
	 *		...weiters k�nnte man auch die Abfrage der Transactional Data zum selektierten
	 *			Hotel mit Button anbringen (#19 - z.B. dass sich ein neues JComboBox-Fenster mit
	 *			Transactions �ffnet wo man alles genauso direkt eingeben und �ndern kann), und dort mit Button f�r Liste
	 *2) Button f�r Statistiken? => Neues Fenster �ffnet sich, dort: Button
	 *			f�r Summary Masterdata und Summary TransactionalData (#1 und #2)
	 *3) MenuItem oder Button f�r Erstellung von Logins f�r HotelRepresentatives?
	 *4)
	 */
	//private JMenuItem jmi_save;
	//private JMenuItem logout;
	//private JTextArea ta_msg;
	//private JMenuItem welcome;
	//private JMenuItem send;
	//private JMenuItem jmi_print;
	private JMenuItem jmi_eMailList;
	private JMenuItem jmi_logins;
	private JButton bt_HoTa_Search;
	private JButton bt_StatChart_Ov;
	
	private String title = "ADRIA_TourismGmbH_SeniorUser_MainMenu";
	private JLabel logo;
	private Icon adria;
	
	public MainWindow_SenUser_ADRIA() {

		initComponents();
		descWindow();
		addToJFrame();
		setJMenuBar(createMenu());
		addAction();

	}

	public void descWindow() {
		setTitle(title);
		setSize(1000, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
		logo = new JLabel(adria);

	}

	public void addAction() {
//		jmi_save.addActionListener(this);
//		logout.addActionListener(this);
//		jmi_print.addActionListener(this);
		jmi_eMailList.addActionListener(this);
		bt_HoTa_Search.addActionListener(this);
		bt_StatChart_Ov.addActionListener(this);
	}

	public void addToJFrame() { //muss noch komplett angepasst/ausgerichtet werden
		
	
		add(logo, BorderLayout.NORTH);
		//add(ta_msg,BorderLayout.NORTH);
		
		add(bt_HoTa_Search, BorderLayout.CENTER);
		//bt_HoTa_Search.setBounds(0, 100, 50, 20);
		add(bt_StatChart_Ov, BorderLayout.AFTER_LAST_LINE);
		//bt_StatChart_Ov//.setBounds(100, 600, 200, 60);
		//(setLocationRelativeTo(bt_HoTa_Search));
	}

	public void initComponents() {
		//ta_msg = new JTextArea();
		//jmi_print = new JMenuItem("Print");
		jmi_eMailList = new JMenuItem("E-Mail Lists");
		bt_HoTa_Search = new JButton("Hotel And Transaction Search");
		bt_StatChart_Ov = new JButton("Statistics And Charts Overview");

	}

	private JMenuBar createMenu() {
		JMenuBar menu = new JMenuBar();

		JMenu file = new JMenu("Menu");
		//jmi_save = new JMenuItem("Save as");
		//send = new JMenuItem("Send");
		//logout = new JMenuItem("Logout");
		jmi_eMailList = new JMenuItem("E-Mail Lists");

		//file.add(jmi_save);
		//file.add(send);
		//file.add(jmi_print);
		//file.addSeparator();
		//file.add(logout);

		JMenu edit = new JMenu("Edit");
		edit.add(jmi_eMailList);
		JMenu help = new JMenu("Help");
		//welcome = new JMenuItem("Welcome");
		//help.add(welcome);

		menu.add(file);
		menu.add(edit);
		menu.add(help);

		return menu;
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		/*if (event.getSource() == jmi_save) {

			String temp = ta_msg.getText();
			// JOptionPane.showMessageDialog(null, "In the area was some text: " + temp);

			try (FileWriter writer = new FileWriter("src/taFile.txt")) {
				writer.write(temp);
			} catch (IOException e) {
				e.printStackTrace();
			}

			JOptionPane.showMessageDialog(null, "Saved");

			ta_msg.setText("");
		} else if (event.getSource() == logout) {
			System.exit(0);
		} else if (event.getSource() == jmi_print) {
			PrintIntro.printComponenet(this);
		} else*/ if (event.getSource() == bt_HoTa_Search) {
			new SearchHotel_ADRIA().setVisible(true);
		} else if (event.getSource() == bt_StatChart_Ov) {
			new StatChartOverview_ADRIA().setVisible(true);
		} else if (event.getSource() == jmi_eMailList) {
			new EMailList_ADRIA().setVisible(true);
		}

	}

}
