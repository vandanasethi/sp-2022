package adriaCode;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import adriaCode.MainWindow_SenUser_ADRIA;
//import adriaCode.MainWindow_HotelRep_ADRIA;

public class LoginWindowADRIA extends JFrame {

		private static String REAL_PWD_SENIORUSER = "senior";
		private static String REAL_USER_SENIORUSER = "senior";

		private static String REAL_PWD_USER = "hotelrep";
		private static String REAL_USER_USER = "hotelrep";

		private JLabel lb_user = new JLabel("User name:");
		private JLabel lb_pwd = new JLabel("Password:");
		private JTextField tf_user = new JTextField(10);
		private JPasswordField tf_pwd = new JPasswordField(10);
		private JButton bt_submit = new JButton("Login");
		
		private JLabel logo;
		private Icon adria;

		public LoginWindowADRIA() {

			setLayout(new FlowLayout());

			setTitle("Login");
			setSize(1000, 500);
			setDefaultCloseOperation(EXIT_ON_CLOSE);
			setLocationRelativeTo(null);
			
			adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
			logo = new JLabel(adria);
			//adria.setImage(adria.getImage().getScaledInstance(100,100,Image.SCALE_DEFAULT));
			
			//Image smallImage = smallImage.getScaledInstance(600, 600, Image.SCALE_DEFAULT);
			
			tf_pwd.setEchoChar('*');

			add(logo);
			add(lb_user);
			add(tf_user);
			add(lb_pwd);
			add(tf_pwd);
			add(bt_submit);

			//pack();

			bt_submit.setEnabled(false);

			tf_user.addCaretListener(new CaretListener() {

				@Override
				public void caretUpdate(CaretEvent e) {
					System.out.println("Cursor was moving");
					if (tf_user.getText().trim().length() >= 4) {
						bt_submit.setEnabled(true);

					} else {
						bt_submit.setEnabled(false);

					}

				}
			});

			bt_submit.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					
					String username = tf_user.getText();
					String pwd = tf_pwd.getText();

					if (username.trim().length() == 0 || pwd.trim().length() == 0) {
						JOptionPane.showMessageDialog(null, "No valid data inside");
						return;
					}

					if (username.equals(REAL_USER_USER) && pwd.equals(REAL_PWD_USER)) {
						new MainWindow_HotelRep_ADRIA().setVisible(true);
						setVisible(false);
					} else if (username.equals(REAL_USER_SENIORUSER) && pwd.equals(REAL_PWD_SENIORUSER)) {
						new MainWindow_SenUser_ADRIA().setVisible(true);
						setVisible(false);
					} else {
						JOptionPane.showMessageDialog(null, "user and /or pwd is incorrect");


					dispose();
					}
				}
					
			});
				
	}

}
