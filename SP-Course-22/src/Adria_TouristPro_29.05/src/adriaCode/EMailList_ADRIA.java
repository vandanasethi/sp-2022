	package adriaCode;

	import java.awt.BorderLayout;
	import java.awt.FlowLayout;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;

	import javax.swing.DefaultComboBoxModel;
	import javax.swing.Icon;
	import javax.swing.ImageIcon;
	import javax.swing.JButton;
	import javax.swing.JComboBox;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;

	public class EMailList_ADRIA extends JFrame {

		private JList<Email> addresses;
		
		private JTextField tf_hotelid;
		private JTextField tf_nrofrooms;
		private JTextField tf_usedrooms;
		private JTextField tf_nrofbeds;
		private JTextField tf_usedbeds;
		private JTextField tf_year;
		private JTextField tf_month;
		private JButton bt_confirm;
		
		private JLabel logo;
		private Icon adria;
		
		public EMailList_ADRIA() {
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			
		//	adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
			logo = new JLabel(adria);

			transactionModel = /*ShowTransactions_ADRIA.getTransactionModel();*/new DefaultComboBoxModel<>();
			cb_transaction = /*ShowTransactions_ADRIA.getCb_transaction();*/new JComboBox<Transaction>(transactionModel);
			//cb_hotel = new JComboBox<Hotel>(hotelModel);
			//cb_hotel = JComboBoxExample.getCb_hotel();
			tf_hotelid = /*ShowTransactions_ADRIA.getTf_hotelid();*/new JTextField(5);
			tf_nrofrooms = new JTextField(5);
			tf_usedrooms = new JTextField(5);
			tf_nrofbeds = new JTextField(5);
			tf_usedbeds = new JTextField(5);
			tf_year = new JTextField(5);
			tf_month = new JTextField(5);
			bt_confirm = new JButton("Confirm Transaction");
			
			setSize(950, 600);
			
			setLayout(new FlowLayout());
			setTitle("Add A New Transaction");
			add(logo, BorderLayout.NORTH);
			add(new JLabel("ID:"));
			add(tf_hotelid);
			//add(cb_hotel);
			add(cb_transaction);
			add(new JLabel("Number of Rooms:"));
			add(tf_nrofrooms);
			add(new JLabel("Used Rooms:"));
			add(tf_usedrooms);
			add(new JLabel("Number of Beds:"));
			add(tf_nrofbeds);
			add(new JLabel("Used Beds:"));
			add(tf_usedbeds);
			add(new JLabel("Year:"));
			add(tf_year);
			add(new JLabel("Month:"));
			add(tf_month);
			add(bt_confirm);

			bt_confirm.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Transaction t = (Transaction) cb_transaction.getSelectedItem();
					
					t.setNumRooms(Integer.valueOf(tf_nrofrooms.getText()));
					t.setUsedRooms(Integer.valueOf(tf_usedrooms.getText()));
					t.setNumBeds(Integer.valueOf(tf_nrofbeds.getText()));
					t.setUsedBeds(Integer.valueOf(tf_usedbeds.getText()));
					t.setYear(Integer.valueOf(tf_year.getText()));
					t.setMonth(Integer.valueOf(tf_month.getText()));
					
					transactionModel.setSelectedItem(t);
					cb_transaction.repaint();
					
				}
			});
			cb_transaction.setModel(transactionModel);
		}
	}

