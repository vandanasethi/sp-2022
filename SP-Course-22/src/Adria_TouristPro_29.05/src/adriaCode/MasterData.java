package adriaCode;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import adriaConnection.ConvertCSV;

public class MasterData {

	private static List<Hotel> all_hotels = new ArrayList<>();
	
	public static List<Hotel> HotelList() {
	
	try {
		all_hotels = ConvertCSV.readHotelFromFile();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
//	hotels.add(new Hotel(661,"*","Gasthof Singula","Strelitzer Daouia","Gschihay Mateeas","Hauptstrasse 22","Payerbach","2650","2853103",62,40));
//	hotels.add(new Hotel(145,"*","Konditorei Massauer","Groebler Trinity","Goetzfried Himat","Boehmgasse 19","Waidhofen","3830","2882571",12,15));
//	hotels.add(new Hotel(55,"***","Hotel garni Steinfeld","Niederhametner Kilian-Noel","Klopstech-Wieter Sakine","Nikolaus-August-Otto-Strasse 4","Wiener Neustadt","2700","3563381",79,100));
//	hotels.add(new Hotel(353,"*","Land- und Seminarpension Gamerith","Tichauer Annamaeria","Mowinckel Luta","Mottingeramt 41","Mottingeramt","3532","2375369",54,131));
//	hotels.add(new Hotel(332,"***","Haus der Pferde","Socjnoha Armella","Koriska Lilli-Valerie","Langschwarza 46","Schrems","3944","4938109",34,7));
	
	return all_hotels;
	}

//	public static List<Hotel> getAll_hotels() {
//		return all_hotels;
//	}
	
}
