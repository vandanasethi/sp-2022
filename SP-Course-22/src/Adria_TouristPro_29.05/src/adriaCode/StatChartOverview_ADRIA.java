package adriaCode;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class StatChartOverview_ADRIA extends JFrame implements ActionListener{
		
	private JButton bt_SummaryMasterDat;
	private JButton bt_SummaryTransDat;
	
	private String title = "ADRIA_TourismGmbH_Statistic_&_Chart_Overview";
	private JLabel logo;
	private Icon adria;
	
	public StatChartOverview_ADRIA() {
		
		setTitle(title);
		setSize(1000, 500);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		
		adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
		logo = new JLabel(adria);
		
		bt_SummaryMasterDat = new JButton("Summary Master Data");
		bt_SummaryTransDat = new JButton("Summary Transactional Data");
		
		add(logo, BorderLayout.NORTH);
		add(bt_SummaryMasterDat, BorderLayout.CENTER);
		add(bt_SummaryTransDat, BorderLayout.AFTER_LAST_LINE);
		
		addAction();
	}
	
	public void addAction() {

		bt_SummaryMasterDat.addActionListener(this);
		//bt_SummaryTransDat.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == bt_SummaryMasterDat) {
			new SummaryMasterDat_ADRIA().setVisible(true);
		}
	}
}
