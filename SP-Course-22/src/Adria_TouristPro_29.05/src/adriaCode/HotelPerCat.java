package adriaCode;

import java.util.ArrayList;

import javax.persistence.Id;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class HotelPerCat {

	@Id
	private Category category;
	@NonNull
	private int noOfEstablishments;
	@NonNull
	private int noOfRooms;
	@NonNull
	private int noOfBeds;
	//private ArrayList<Category> categories;
	
	
	public HotelPerCat(Category categoryInp) {
		this.category = categoryInp;
		this.noOfEstablishments = 0;
		this.noOfRooms = 0;
		this.noOfBeds = 0;
	}

	
//	public HotelPerCat CountCategories(ArrayList<Hotel> hotelInp, HotelPerCat categoryInp) {
//		
//		for(Hotel hotel : hotelInp) {
//			if(hotel.getCategory() == categoryInp.category) {
//				categoryInp.noOfEstablishments++;
//				categoryInp.noOfRooms += categoryInp.noOfRooms;
//				categoryInp.noOfBeds += categoryInp.noOfBeds;
//			}
//		}
//		return categoryInp;
//	}
}
