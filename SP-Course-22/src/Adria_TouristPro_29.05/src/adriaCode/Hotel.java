package adriaCode;

public class Hotel {

	private int id;
	private Category category;
	private String name;
	private String owner;
	private String contact;
	private String address;
	private String city;
	private String citycode;
	private String phone;
	private int numRooms;
	private int numBeds;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCitycode() {
		return citycode;
	}
	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getNumRooms() {
		return numRooms;
	}
	public void setNumRooms(int numRooms) {
		this.numRooms = numRooms;
	}
	public int getNumBeds() {
		return numBeds;
	}
	public void setNumBeds(int numBeds) {
		this.numBeds = numBeds;
	}
	public Hotel(int id, Category category, String name, String owner, String contact, String address, String city,
			String citycode, String phone, int numRooms, int numBeds) {
		super();
		this.id = id;
		this.category = category;
		this.name = name;
		this.owner = owner;
		this.contact = contact;
		this.address = address;
		this.city = city;
		this.citycode = citycode;
		this.phone = phone;
		this.numRooms = numRooms;
		this.numBeds = numBeds;
	}
	@Override
	public String toString() {
		return name;
	}
	public String hotelToCSV() {
		char a = '"';
		String sep = a + "," + a;
		
		return this.id + "," + a +
		this.category + sep +
		this.name + sep +
		this.owner + sep +
		this.contact + sep +
		this.address + sep +
		this.city + sep +
		this.citycode + sep +
		this.phone + a + "," +
		this.numRooms + "," +
		this.numBeds;
	}
}
