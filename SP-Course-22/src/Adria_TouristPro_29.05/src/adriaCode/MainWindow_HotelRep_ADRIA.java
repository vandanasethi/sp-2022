package adriaCode;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

public class MainWindow_HotelRep_ADRIA extends JFrame implements ActionListener {

	private JMenuItem jmi_save;
	private JMenuItem exit;
	private JTextArea ta_msg;
	private JMenuItem welcome;
	private JMenuItem open;
	private JButton bt_submit;
	private JMenuItem jmi_print;
	
	private JLabel logo;
	private Icon adria;

	public MainWindow_HotelRep_ADRIA() {

		initComponents();
		descWindow();
		addToJFrame();
		setJMenuBar(createMenu());
		addAction();

	}

	public void descWindow() {
		setTitle("ADRIA_TourismGmbH_HotelRepresentative_MainMenu");
		setSize(1000, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
		logo = new JLabel(adria);

	}

	public void addAction() {
		jmi_save.addActionListener(this);
		exit.addActionListener(this);
		jmi_print.addActionListener(this);
	}

	public void addToJFrame() {
		
		add(logo, BorderLayout.NORTH);
		//add(ta_msg,BorderLayout.NORTH);
		
		add(new JButton("Add Transaction"),BorderLayout.EAST);
		add(new JButton("View Transactions"),BorderLayout.CENTER);
		add(new JButton("Hotel Information"),BorderLayout.WEST);

	
	

	}

	public void initComponents() {
		ta_msg = new JTextArea();
		bt_submit = new JButton("Submit");
		jmi_print = new JMenuItem("Print");

	}

	private JMenuBar createMenu() {
		JMenuBar menu = new JMenuBar();

		JMenu file = new JMenu("Menu");
		jmi_save = new JMenuItem("Save as");
		 open = new JMenuItem("Open as");
		exit = new JMenuItem("Exit");

		file.add(jmi_save);
		file.add(open);
		file.add(jmi_print);
		file.addSeparator();
		file.add(exit);

		JMenu edit = new JMenu("Edit");
		JMenu help = new JMenu("help");
		welcome = new JMenuItem("Welcome");
		help.add(welcome);

		menu.add(file);
		menu.add(edit);
		menu.add(help);

		return menu;
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		if (event.getSource() == jmi_save) {

			String temp = ta_msg.getText();
			// JOptionPane.showMessageDialog(null, "In the area was some text: " + temp);

			try (FileWriter writer = new FileWriter("src/taFile.txt")) {
				writer.write(temp);
			} catch (IOException e) {
				e.printStackTrace();
			}

			JOptionPane.showMessageDialog(null, "Saved");

			ta_msg.setText("");
		} else if (event.getSource() == exit) {
			System.exit(0);
		} else if (event.getSource() == jmi_print) {
			PrintIntro.printComponenet(this);
		}

	}

}
