package adriaCode;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ShowTransactions_ADRIA extends JFrame {

	private JComboBox<Transaction> cb_transaction;
	//private JComboBox<Hotel> cb_hotel;
	private DefaultComboBoxModel<Transaction> transactionModel;
	//private DefaultComboBoxModel<Hotel> hotelModel;
	private JTextField tf_hotelid;
	private JTextField tf_nrofrooms;
	private JTextField tf_usedrooms;
	private JTextField tf_nrofbeds;
	private JTextField tf_usedbeds;
	private JTextField tf_year;
	private JTextField tf_month;
	private JButton bt_addNewTransaction;
	private JButton bt_backupTransactions;
	
	private JLabel logo;
	private Icon adria;	


	public ShowTransactions_ADRIA() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
		logo = new JLabel(adria);

		transactionModel = new DefaultComboBoxModel<>();
		cb_transaction = new JComboBox<Transaction>(transactionModel);
		//cb_hotel = new JComboBox<Hotel>(hotelModel);
		//cb_hotel = JComboBoxExample.getCb_hotel();
		tf_hotelid = new JTextField(5);
		tf_nrofrooms = new JTextField(5);
		tf_usedrooms = new JTextField(5);
		tf_nrofbeds = new JTextField(5);
		tf_usedbeds = new JTextField(5);
		tf_year = new JTextField(5);
		tf_month = new JTextField(5);
		bt_addNewTransaction = new JButton("Add New Transaction");
		bt_backupTransactions = new JButton("Save Backup Transactions");
		setSize(950, 600);
		//setLocationRelativeTo(c);
		
		setLayout(new FlowLayout());
		setTitle("Transactions Of A Hotel");
		add(logo, BorderLayout.NORTH);
		add(new JLabel("ID:"));
		add(tf_hotelid);
		tf_hotelid.setEnabled(false);
		//add(cb_hotel);
		add(cb_transaction);
		add(new JLabel("Number of Rooms:"));
		add(tf_nrofrooms);
		add(new JLabel("Used Rooms:"));
		add(tf_usedrooms);
		add(new JLabel("Number of Beds:"));
		add(tf_nrofbeds);
		add(new JLabel("Used Beds:"));
		add(tf_usedbeds);
		add(new JLabel("Year:"));
		add(tf_year);
		add(new JLabel("Month:"));
		add(tf_month);
		add(bt_addNewTransaction);
		add(bt_backupTransactions);
		
		List<Transaction> transactions = new ArrayList<>();
		
		transactions.add(new Transaction(566,44,13,220,220,2014,1));
		transactions.add(new Transaction(615,30,28,21,21,2014,1));
		transactions.add(new Transaction(98,58,58,74,13,2014,1));
		transactions.add(new Transaction(14,2,1,19,19,2014,1));
		transactions.add(new Transaction(308,23,6,127,18,2014,1));
		transactions.add(new Transaction(110,46,3,40,12,2014,1));
		transactions.add(new Transaction(676,10,6,18,18,2014,1));
		
		cb_transaction.setModel(transactionModel);
		
		for (Transaction currentTransaction : transactions) {
			transactionModel.addElement(currentTransaction);
		}
		
		
		cb_transaction.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Transaction t = (Transaction) transactionModel.getSelectedItem();

				System.out.println(t);

				//tf_hotelid.setText("" + t.getId());
				tf_nrofrooms.setText("" + t.getNumRooms());
				tf_usedrooms.setText(""+ t.getUsedRooms());
				tf_nrofbeds.setText("" + t.getNumBeds());
				tf_usedbeds.setText("" + t.getUsedBeds());
				tf_year.setText("" + t.getYear());
				tf_month.setText("" + t.getMonth());
								
				
				//tf_gender.setText(t.getGender().toString());
				//cbtravelAlone.setSelected(t.isTravelAlone());

			}
		});
		bt_addNewTransaction.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//Hotel h = (Hotel) cb_hotel.getSelectedItem();
				//h.setId(Integer.valueOf(tf_hotelid.getText()));
				new AddNewTransaction_ADRIA().setVisible(true);
				//AddTransDat_ADRIA.cb_hotel.setCb_hotel(h) ---- Auf AddTransDat w�re eine JCB zu installieren
			}
		});
		bt_backupTransactions.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				JFileChooser jfc = new JFileChooser();
				int answer = jfc.showSaveDialog(null);
				System.out.println("checkpoint: " + answer);
				String path = null;

				if (answer == JFileChooser.APPROVE_OPTION) {
					path = jfc.getSelectedFile().getAbsolutePath();
					System.out.println(path);
				}

				String timestamp = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
				File f1 = new File(path + timestamp);
				//File f1 = new File("src/adriaCode/BackupOccDat" + timestamp);

				try (BufferedWriter writer = new BufferedWriter(new FileWriter(f1))) {
					for (Transaction transaction : transactions) { //Hotel.getALL_HOTELS()
		
						writer.write(transaction.transactionToCSV() + "\n");
					}
		
				} catch (Exception exc) {
					System.err.println(exc.getLocalizedMessage());
				}
			}

		});

	}
	
}
