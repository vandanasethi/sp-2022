package adriaConnection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFileChooser;

import adriaCode.Category;
import adriaCode.Hotel;

public class ConvertCSV {

	public static void main(String[] args) {

	List<Hotel> all_hotels = new ArrayList<>();

	try {
		all_hotels = readHotelFromFile();

		} catch (FileNotFoundException e) {
		System.err.println(e.getMessage());
		}

			

		writeHotelToCSV(all_hotels);

		}

	public static void writeHotelToCSV(List<Hotel> all_hotels) {

			JFileChooser jfc = new JFileChooser();
			int answer = jfc.showSaveDialog(null);
			System.out.println("checkpoint: " + answer);
			String path = null;

			if (answer == JFileChooser.APPROVE_OPTION) {
				path = jfc.getSelectedFile().getAbsolutePath();
				System.out.println(path);
			}

			//File f1 = new File(path);
			File f1 = new File("src/adriaProjectConnection/copy.txt");

			try (BufferedWriter writer = new BufferedWriter(new FileWriter(f1))) {
				for (Hotel hotel : all_hotels) { //Hotel.getALL_HOTELS()
	
					writer.write(hotel.hotelToCSV() + "\n");
				}
	
			} catch (Exception e) {
				System.err.println(e.getLocalizedMessage());
			}
		}

		public static List<Hotel> readHotelFromFile() throws FileNotFoundException {

			ArrayList<Hotel> all_hotels = new ArrayList<>();

			String path = "src/adriaConnection/hotels.txt";

			File f = new File(path); // null

			try (Scanner scan = new Scanner(f, "UTF-8")) {
				while (scan.hasNext()) {

					String[] parts = scan.nextLine().split(",");

					String id = parts[0];
					String category = parts[1];
					String name = parts[2];
					String owner = parts[3];
					String contact = parts[4];
					String address = parts[5];
					String city = parts[6];
					String citycode = parts[7];
					String phone = parts[8];
					String numRooms = parts[9];
					String numBeds = parts[10];
					
//					char x = '"';
//					char y = '*';
//					String bla = "" + x + x + y + x + x;
//					System.out.println(bla);
					
					Category catEnumerated = null;
					if (/*category.equals(bla)*/category.length() == 3) {
						catEnumerated = Category.ONE;
						//System.out.println(true);//catEnumerated);
						//setCategory(Category.ONE);= Category.ONE;
						
					} else if (/*category.equals("**")*/category.length() == 4) {
						catEnumerated = Category.TWO;
						
					} else if (/*category.equals("***")*/category.length() == 5) {
						catEnumerated = Category.THREE;
						
					} else if (/*category.equals("****")*/category.length() == 6) {
						catEnumerated = Category.FOUR;
					
					} else if (/*category.equals("*****")*/category.length() == 7) {
						catEnumerated = Category.FIVE;
						//System.out.println(name + ", " + category + ", " + numRooms + ", " + numBeds);
					}
					
//					if(category == (bla)) {
//						System.out.println("Heeeeeeeeeeeeaaaaaaaaaaaassssssssst");
//					}
//					
//					System.out.println(category.toCharArray());
//					char [] c = category.toCharArray();
//					System.out.println(c[0]);
//					System.out.println(c[1]);
//					System.out.println(c[2]);
//					if (c[1] == '*') {
//						System.out.println(true);
//					}
//					System.out.println(catEnumerated);

					all_hotels.add(new Hotel(Integer.valueOf(id), catEnumerated, name, owner, contact, address, city, citycode, phone, Integer.valueOf(numRooms), Integer.valueOf(numBeds)));

				}
			} catch (Exception e) {
				System.err.println("Reading");
			}
			all_hotels.forEach(System.out::println);
			return all_hotels;
		}

}
