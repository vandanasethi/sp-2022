package transaction;

import javax.swing.JComboBox;

public class Transaction {

	
	private int id;
	private int numRooms;
	private int usedRooms;
	private int numBeds;
	private int usedBeds;
	private int year;
	private int month;
	
	public Transaction(int id, int numRooms, int usedRooms, int numBeds, int usedBeds, int year, int month) {
		super();
		this.id = id;
		this.numRooms = numRooms;
		this.usedRooms = usedRooms;
		this.numBeds = numBeds;
		this.usedBeds = usedBeds;
		this.year = year;
		this.month = month;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumRooms() {
		return numRooms;
	}
	public void setNumRooms(int numRooms) {
		this.numRooms = numRooms;
	}
	public int getUsedRooms() {
		return usedRooms;
	}
	public void setUsedRooms(int usedRooms) {
		this.usedRooms = usedRooms;
	}
	public int getNumBeds() {
		return numBeds;
	}
	public void setNumBeds(int numBeds) {
		this.numBeds = numBeds;
	}
	public int getUsedBeds() {
		return usedBeds;
	}
	public void setUsedBeds(int usedBeds) {
		this.usedBeds = usedBeds;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	@Override
	public String toString() {
		return "Hotel-ID: " + id + "| Year: " + year + "| Month: " + month;
	}
	public String transactionToCSV() {
				
		return this.id + "," +
		this.numRooms + "," +
		this.usedRooms + "," +
		this.numBeds + "," +
		this.usedBeds + "," +
		this.year + "," +
		this.month;
		
	}
	
}
