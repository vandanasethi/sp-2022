package transaction;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import connect.Hotel;
import connect.ODAO;
import connect.Occupancy;

public class ShowTransactions_ADRIA extends JFrame {

    ArrayList<Occupancy> transactionsFromHotel = new ArrayList<>();
    //Hotel thisHotel = SearchHotel_ADRIA.h();

    //JPanel machen mit Hotelname und Abfrage, neues JPanel mit JTable!

    private JComboBox<Occupancy> cb_transaction; 
    private DefaultComboBoxModel<Occupancy> transactionModel;
    private JComboBox<Hotel> cb_hotel = new JComboBox<Hotel>(functions.SearchHotel_ADRIA.getModel()); //tryout
    private static DefaultComboBoxModel<Hotel> hotelModel; //tryout
    private JTextField tf_hotelid;
    private JTextField tf_nrofrooms;
    private JTextField tf_usedrooms;
    private JTextField tf_nrofbeds;
    private JTextField tf_usedbeds;
    private JTextField tf_year;
    private JTextField tf_month;
    private JButton bt_addNewTransaction;
    private JButton bt_backupTransactions;

    private JLabel logo;// = Logo.getLogo();
    private Icon adria;    

    private List<Occupancy> transactions = (List<Occupancy>) ODAO.getAllOccupancies();

    /*public getTransactionsFromHotel() {
        if (SearchHotel.h )
    };*/

    public ShowTransactions_ADRIA() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
        logo = new JLabel(adria);

        transactionModel = new DefaultComboBoxModel<>();
        //cb_hotel = new JComboBox<Hotel>(SearchHotel_ADRIA.getModel());
        cb_transaction = new JComboBox<Occupancy>(transactionModel);
        tf_hotelid = new JTextField(5);
        //tf_hotelid.get(SearchHotel_ADRIA.h.tf_hotelid);
        tf_nrofrooms = new JTextField(5);
        tf_usedrooms = new JTextField(5);
        tf_nrofbeds = new JTextField(5);
        tf_usedbeds = new JTextField(5);
        tf_year = new JTextField(5);
        tf_month = new JTextField(5);
        bt_addNewTransaction = new JButton("Add New Transaction");
        bt_backupTransactions = new JButton("Save Backup Transactions");
        setSize(950, 600);
        setLocationRelativeTo(null);


        setLayout(new FlowLayout());
        setTitle("Transactions Of A Hotel");
        add(logo, BorderLayout.NORTH);
        add(cb_hotel).setEnabled(false);;
        add(new JLabel("ID:"));
        add(tf_hotelid);
        tf_hotelid.setEnabled(false);
//        add(cb_transaction);
//        cb_transaction.setEnabled(false);
        add(new JLabel("Number of Rooms:"));
        add(tf_nrofrooms);
        add(new JLabel("Used Rooms:"));
        add(tf_usedrooms);
        add(new JLabel("Number of Beds:"));
        add(tf_nrofbeds);
        add(new JLabel("Used Beds:"));
        add(tf_usedbeds);
        add(new JLabel("Year:"));
        add(tf_year);
        add(new JLabel("Month:"));
        add(tf_month);
        add(bt_addNewTransaction);
        add(bt_backupTransactions);

        
        cb_transaction.setModel(transactionModel);

        for (Occupancy currentTransaction : transactions) {
            transactionModel.addElement(currentTransaction);
        }


        cb_transaction.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                Occupancy t = (Occupancy) transactionModel.getSelectedItem();

                System.out.println(t);

                //tf_hotelid.setText("" + t.getId());
                tf_nrofrooms.setText("" + t.getRooms());
                tf_usedrooms.setText(""+ t.getUsedRooms());
                tf_nrofbeds.setText("" + t.getBeds());
                tf_usedbeds.setText("" + t.getUsedBeds());
                tf_year.setText("" + t.getYear());
                tf_month.setText("" + t.getMonth());


                //tf_gender.setText(t.getGender().toString());
                //cbtravelAlone.setSelected(t.isTravelAlone());

            }
    });
        bt_addNewTransaction.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //Hotel h = (Hotel) cb_hotel.getSelectedItem();
                //h.setId(Integer.valueOf(tf_hotelid.getText()));
                new AddNewTransaction_ADRIA().setVisible(true);
                //AddTransDat_ADRIA.cb_hotel.setCb_hotel(h) ---- Auf AddTransDat w�re eine JCB zu installieren
            }
        });
        bt_backupTransactions.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                String timestamp = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
                File f1 = new File("src/adriaCode/BackupOccDat" + timestamp); 

                try (BufferedWriter writer = new BufferedWriter(new FileWriter(f1))) {
                    for (Occupancy transaction : transactions) { //Hotel.getALL_HOTELS()

                        writer.write(transaction/*.transactionToCSV()*/ + "\n");
                    }

                } catch (Exception exc) {
                    System.err.println(exc.getLocalizedMessage());
                }
            }

        });

    }
}

    