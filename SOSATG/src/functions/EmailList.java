//package functions;
//										
//import java.awt.BorderLayout;
//import java.awt.Color;
//import java.awt.Dimension;
//import java.awt.FlowLayout;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.Properties;
//
// 
//
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.PasswordAuthentication;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeBodyPart;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;
//import javax.swing.Icon;
//import javax.swing.ImageIcon;
//import javax.swing.JButton;
//import javax.swing.JFrame;
//import javax.swing.JLabel;
//import javax.swing.JOptionPane;
//import javax.swing.JPanel;
//import javax.swing.JTable;
//import javax.swing.table.DefaultTableModel;
//
//import connect.HDAO;
//import connect.Hotel;
//
// 
//public class EmailList extends JFrame {
//
// 
//
//    JPanel top_panel;
//    JPanel bottom_panel;
//    private JLabel logo;
//    private Icon adria;    
//    private JButton bt_addReceipient;
//    private JButton bt_deleteReceipient;
//    private JButton bt_send;
//    
//    ArrayList<EmailReceiver> receiverList = new ArrayList<EmailReceiver>();
//
// 
//
//    public EmailList() {
//
// 
//
//        //setLayout(new FlowLayout());
//        setSize(600, 400);
//        setLocationRelativeTo(null);
//        setDefaultCloseOperation(EXIT_ON_CLOSE);
//        
////        adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
////        logo = new JLabel(adria);
//        bt_addReceipient = new JButton("Add New Receipient");
//        bt_deleteReceipient = new JButton("Delete Receipient");
//        bt_send = new JButton("Send E-Mail");
//        
//        top_panel = new JPanel();
//        top_panel.setPreferredSize(new Dimension(700, 200));
//        
//        bottom_panel = new JPanel();
//        bottom_panel.setPreferredSize(new Dimension(300, 100));
//        bottom_panel.setBackground(Color.black);
//        
////        top_panel.add(new JLabel("out of the top panel area"));
////
////        add(new JLabel("HI"), BorderLayout.SOUTH);
////        add(logo, BorderLayout.NORTH);
//        
//        String[] columns = {/*"ID", */"Name", "Surname", "Mail"};        
//        DefaultTableModel model = new DefaultTableModel();
//        
//        receiverList.add(new EmailReceiver ("Vandana", "Sethi", "fhb201482@fh-vie.ac.at"));/// {ONE, TWO, THREE, FOUR, FIVE}; Array hat noch keinen Inhalt. M�ssen aber nur 
//        receiverList.add(new EmailReceiver ("Micah", "Ongcoy", "fhb201476@fh-vie.ac.at"));
//        receiverList.add(new EmailReceiver ("Alaa", "Salama", "fhb201479@fh-vie.ac.at"));
//        receiverList.add(new EmailReceiver ("Florian", "Haider", "fhb191285@fh-vie.ac.at"));
//        
//        //for (EmailReceiver receiver : receiverList) {
//        //    CountCategories((List<Hotel>)HDAO.getAllHotels(), hotel);
//        //}
//        
//        for (String column : columns) {
//            model.addColumn(column);
//        }
//        model.addRow(new Object[] {/*"ID" , */"Name", "Surname", "Email-Address"});
//        for (EmailReceiver receiver : receiverList) {
//            model.addRow(new Object[] {/*receiver.getId(), */receiver.getName(), receiver.getSurname(), receiver.getEMailAddress()});
//    
//            JTable table = new JTable(model);
//            /*top_panel.*/add(table);
//            //add(top_panel, BorderLayout.NORTH);
//            bottom_panel.add(bt_addReceipient);
//            bottom_panel.add(bt_deleteReceipient);
//            bottom_panel.add(bt_send);
//            add(bottom_panel, BorderLayout.SOUTH);
//           
//            setVisible(true);
//           
//            bt_addReceipient.addActionListener(new ActionListener() {
//               
//                @Override
//                public void actionPerformed(ActionEvent e) {
//                   
//                    EmailReceiver receipient = new EmailReceiver();
//                   
//                    String name = JOptionPane.showInputDialog("Prename");
//                    if ((name == null) || name.trim().equals("")) {
//                        JOptionPane.showMessageDialog(null, "Please enter a valid Prename");
//                        return;
//                    } else {
//                        receipient.setName(name);               
//                        String surname = JOptionPane.showInputDialog("Surname");
//                        if ((surname == null) || surname.trim().equals("")) {
//                            JOptionPane.showMessageDialog(null, "Please enter a valid Surname");
//                            return;
//                        } else {
//                            receipient.setSurname(surname);
//                            String mail = JOptionPane.showInputDialog("E-Mail Address");
//                            if ((mail == null) || mail.trim().equals("")) {
//                                JOptionPane.showMessageDialog(null, "Please enter a valid E-Mail Address");
//                                return;
//                            } else if (!(mail.contains("@"))) {
//                                JOptionPane.showMessageDialog(null, "The E-Mail adress is incorrect");
//                                return;
//                            } else {
//                            //    to.add("Michael.Deutsch@fh-vie.ac.at");
//                                receipient.setEMailAddress(mail);
//                                receiverList.add(receipient); //Neue Mails hinzuf�gen
//                               
//                                model.addRow(new Object[] {/*receiver.getId(), */receipient.getName(), receipient.getSurname(), receipient.getEMailAddress()});
//                                table.repaint();
//                            }
//                        }
//                    }
//                }
//               
//            });
//           
////            bt_deleteReceipient.addActionListener(new ActionListener() {
////               
////                @Override
////                public void actionPerformed(ActionEvent e) {
////                   
////                    EmailReceiver receipient = new EmailReceiver();
////                   
////                    String name = JOptionPane.showInputDialog("Prename");
////                    String surname = JOptionPane.showInputDialog("Surname");
////                    for (EmailReceiver receiver : receiverList) {
////                        if ((name == receiver.getName() && surname == receiver.getSurname())) {
////                            JOptionPane.showConfirmDialog(null, "Do you want to delete the Mail-Address " + receiver.getEMailAddress() + " of " + receiver.getName() + " " + receiver.getSurname() + "?",
////                                    "Select an Option" , JOptionPane.YES_NO_OPTION);
////                           
////                            if (deleteConfirmation == 0) {
////                                model.removeElement(h);
////                               
////                            }
////                            //JOptionPane.showMessageDialog(null, "Please enter the Prename you want to delete");
////                            return;
////                        }
////                    }
////                    else {
////                        receipient.setName(name);               
////                        String surname = JOptionPane.showInputDialog("Surname");
////                        if ((surname == null) || surname.trim().equals("")) {
////                            JOptionPane.showMessageDialog(null, "Please enter the Surname you want to delete");
////                            return;
////                        } else {
////                            receipient.setSurname(surname);
////                           
////                            receiverList.add(receipient); //Neue Mails hinzuf�gen
////                           
////                            //model.removeRow(row);
////                            model.addRow(new Object[] {/*receiver.getId(), */receipient.getName(), receipient.getSurname(), receipient.getEMailAddress()});
////                            table.repaint();
////                        }
////                    }
////                }
////               
////            });  
//            bt_send.addActionListener(new ActionListener() {
//                
//                @Override
//                public void actionPerformed(ActionEvent e) {
//
//                    String user = "AdriaTourismGmbH@gmx.net";
//                    String pwd = "PiazziMa";                 
//                    String host = "mail.gmx.net";
//                                    
//                    Properties properties = System.getProperties();
//                    
//                    properties.put("mail.smtp.auth", "true");
//                    properties.put("mail.smtp.starttls.enable", "true");
//                    properties.put("mail.smtp.host", host);
//                    properties.put("mail.smtp.port", "587");
//                    JOptionPane.showMessageDialog(null, "Empf�nger hinzuf�gen");
//                    
//                    Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
//                        
//                        protected PasswordAuthentication getPasswordAuthentication() {
//                            
//                            return new PasswordAuthentication(user, pwd);
//                        }
//                    });
//                    
//                    try {
//                        MimeMessage message = new MimeMessage(session);
//                        
//                        message.setFrom(new InternetAddress(user));
//                        for (int i = 0; i < receiverList.size(); i++) {
//                            message.addRecipient(Message.RecipientType.TO, new InternetAddress(receiverList.get(i).getEMailAddress()));
//                        }
//                            
//                        
//                        String timestamp = new SimpleDateFormat("yyyyMMdd'.txt'").format(new Date());
//                        message.setSubject("Master Data " + timestamp);
//                        //message.setText(text);
//                        
//                        MimeMultipart multipart = new MimeMultipart();
//                        MimeBodyPart attachmentPart = new MimeBodyPart();
//                        
//                        File f2 = new File("src/E-Mail_" + timestamp);
//
//     
//
//                        try (BufferedWriter writer = new BufferedWriter(new FileWriter(f2))) {
//                            for (Hotel hotel : HDAO.getAllHotels()) { 
//                
//                                writer.write(hotel + "\n");
//                            }
//                
//                        } catch (Exception exc) {
//                            System.err.println(exc.getLocalizedMessage());
//                        }
//                        attachmentPart.attachFile(f2);
//                        multipart.addBodyPart(attachmentPart);
//                        message.setContent(multipart); 
//                        
//                        Transport.send(message);
//                        
//                    } catch (MessagingException ex) {
//                        ex.printStackTrace();
//                    } catch (IOException ioex) {
//                        ioex.printStackTrace();
//                    }
//                }
//            });
//
//     
//
//        }
//        
//        
////        private Object[][] convertIntoArray(ArrayList<Studento> names) {
//    //
////            Object[][] students = new Object[names.size()][];
////            int pos = 0; 
////            
////            for (Studento currentStudent : names) {
////                students[pos] = new Object[] {currentStudent.getId(), currentStudent.getName(), currentStudent.getGrad()};
////                pos++;
////            }
////            
////            
////            return students;
////        }
//
//     
//
//    }
//  
//            
//  }