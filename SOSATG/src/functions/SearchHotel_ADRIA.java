package functions;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException; 
import javax.mail.Transport;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.microsoft.sqlserver.jdbc.StringUtils;

import connect.HDAO;
import connect.HData;
import connect.Hotel;
import overview.Category;

public class SearchHotel_ADRIA extends JFrame {

	private JComboBox<Hotel> cb_hotel;
	private static DefaultComboBoxModel<Hotel> model;
	private JTextField tf_hotelid;
	private JTextField tf_hotelcat; 
	private JTextField tf_hotelname;
	private JTextField tf_hotelowner;
	private JTextField tf_contact;
	private JTextField tf_address;
	private JTextField tf_city;
	private JTextField tf_citycode;
	private JTextField tf_phone;
	private JTextField tf_nrofrooms;
	private JTextField tf_nrofbeds;
	private JButton bt_editHotel;
	private JButton bt_addNewHotel;
	private JButton bt_showRelatedTransactions;
	private JButton bt_deleteHotel;
	private JButton bt_backupHotels;
	private JButton bt_sendEmail;
	
	private JLabel logo;
	private Icon adria;	
	
	private List<Hotel> hotels = (List<Hotel>) HDAO.getAllHotels();
	private static int idCount = HDAO.getAllHotels().size() + 1;

	public SearchHotel_ADRIA() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
		logo = new JLabel(adria);
		
		model = new DefaultComboBoxModel<>();
		cb_hotel = new JComboBox<>(model);
		tf_hotelid = new JTextField(5);
		tf_hotelcat = new JTextField(5);
		tf_hotelname = new JTextField(20);
		tf_hotelowner = new JTextField(15);
		tf_contact = new JTextField(15);
		tf_address = new JTextField(20);
		tf_city = new JTextField(10);
		tf_citycode = new JTextField(10);
		tf_phone = new JTextField(10);
		tf_nrofrooms = new JTextField(5);
		tf_nrofbeds = new JTextField(5);
		
		bt_editHotel = new JButton("Edit Hotel");
		bt_addNewHotel = new JButton("Add A New Hotel");
		bt_showRelatedTransactions = new JButton("Show Related Transactions");
		bt_deleteHotel= new JButton("Delete This Hotel");
		bt_backupHotels = new JButton("Save Backup Hotels");
		bt_sendEmail = new JButton("Send E-Mail");
		setSize(950, 570);
		setLocationRelativeTo(null);

		setLayout(new FlowLayout());
		setTitle("Hotel And Transaction Search");
		add(logo, BorderLayout.NORTH);
		add(new JLabel("ID:"));
		add(tf_hotelid);
		tf_hotelid.setEnabled(false);
		add(cb_hotel);
		add(new JLabel("Category:"));
		add(tf_hotelcat);
		add(new JLabel("Name:"));
		add(tf_hotelname);
		add(new JLabel("Owner:"));
		add(tf_hotelowner);
		add(new JLabel("Contact:"));
		add(tf_contact);
		add(new JLabel("Address:"));
		add(tf_address);
		add(new JLabel("City:"));
		add(tf_city);
		add(new JLabel("Citycode:"));
		add(tf_citycode);
		add(new JLabel("Phone:"));
		add(tf_phone);
		add(new JLabel("Number Of Rooms:"));
		add(tf_nrofrooms);
		add(new JLabel("Number Of Beds:"));
		add(tf_nrofbeds);
		add(bt_editHotel);
		add(bt_addNewHotel);
		add(bt_showRelatedTransactions);
		add(bt_deleteHotel);
		add(bt_backupHotels);
		add(bt_sendEmail);

		cb_hotel.setModel(model);

		for (Hotel currentHotel : HDAO.getAllHotels()) {
			model.addElement(currentHotel);
			if (currentHotel.getId() >= getIdCount()) {
				setIdCount(currentHotel.getId() + 1);
			}
		}
		System.out.println(getModel());

		cb_hotel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Hotel h = (Hotel) model.getSelectedItem();

				System.out.println(h);

				tf_hotelid.setText("" + h.getId());
				tf_hotelcat.setText("" + h.getCategory()); 
				tf_hotelname.setText(h.getName());
				tf_hotelowner.setText(h.getOwner());
				tf_contact.setText(h.getContact());
				tf_address.setText(h.getAddress());
				tf_city.setText(h.getCity());
				tf_citycode.setText(h.getCitycode());
				tf_phone.setText(h.getPhone());
				tf_nrofrooms.setText("" + h.getNumRooms());
				tf_nrofbeds.setText("" + h.getNumBeds());
	
			}
		});
		
		bt_editHotel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Hotel h = (Hotel) cb_hotel.getSelectedItem();
				
				String category = tf_hotelcat.getText();
                if (category.equals("*")) {
                    h.setCategory(Category.ONE);     
                } else if (category.equals("**")) {
                    h.setCategory(Category.TWO);
                } else if (category.equals("***")) {
                    h.setCategory(Category.THREE);
                } else if (category.equals("****")) {
                    h.setCategory(Category.FOUR);
                } else if (category.equals("*****")) {
                    h.setCategory(Category.FIVE);
                } else {
                    JOptionPane.showMessageDialog(null, "'Category:' must have a value from * to *****");
                }


				if (tf_hotelname.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Name:' cannot be empty");
					return;
				} else {
					h.setName(tf_hotelname.getText());
				}
				
				if (tf_hotelowner.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Owner:' cannot be empty");
					return;
				} else {
					h.setOwner(tf_hotelowner.getText());
				}
				
				if (tf_contact.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Contact:' cannot be empty");
					return;
				} else {
					h.setContact(tf_contact.getText());
				}
				
				if (tf_address.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Address:' cannot be empty");
					return;
				} else {
					h.setAddress(tf_address.getText());
				}
				
				if (tf_city.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'City:' cannot be empty");
					return;
				} else {
					h.setCity(tf_city.getText());
				}
				
				if (tf_citycode.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Citycode:' cannot be empty");
					return;
				} else {
					h.setCitycode(tf_citycode.getText());
				}
				
				if (tf_phone.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Phone:' cannot be empty");
					return;
				} else {
					h.setPhone(tf_phone.getText());
				}
				
				if (StringUtils.isInteger(tf_nrofrooms.getText())) {
					h.setNumRooms(Integer.valueOf(tf_nrofrooms.getText()));
				} else {
					JOptionPane.showMessageDialog(null, "'Number Of Rooms:' must be a number");
					return;
				}
				
				if (StringUtils.isInteger(tf_nrofbeds.getText())) {
					h.setNumBeds(Integer.valueOf(tf_nrofbeds.getText()));
				} else {
					JOptionPane.showMessageDialog(null, "'Number Of Beds:' must be a number");
					return;
				}

				model.setSelectedItem(h);	//im Programm werden die Ver�nderungen gemacht 
				HDAO.update(h);	//DB ge�ndert
				cb_hotel.repaint();		//Visuell im JavaSwing sofort ge�ndert sichtbar sind 
			}
		});
		
		
		bt_addNewHotel.addActionListener(new ActionListener() { 

			@Override
			public void actionPerformed(ActionEvent e) {
				
				new AddHotel().setVisible(true);
			}
		});
		
		/**
		 * Autorin: Micah Ongcoy 
		 * @param h ist das vom User ausgewaehlte Hotel in der "Hotel und Transactions Search" Dropdown Liste. 
		 * In der Methode wird erscheint ein neues Fenster mit der Abfrage, ob das Hotel wirklich gel�scht 
		 * werden soll. Der User hat die M�glichkeit zwischen "Ja" oder "Nein". 
		 * Wenn "Ja" ausgew�hlt wird, wird das Hotel in der JComboBox entfernt und anschlie�end in der Datenbank 
		 * gel�scht. 
		 * 
		 */
		
		bt_deleteHotel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Hotel h = (Hotel) model.getSelectedItem();
				
				int deleteConfirmation = JOptionPane.showConfirmDialog(null, "Do you want to delete this Hotel?", "Select an Option" , JOptionPane.YES_NO_OPTION);
				if (deleteConfirmation == 0) {
					model.removeElement(h);
					HDAO.delete(h);
				}
			}
		});
		
		
		bt_showRelatedTransactions.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Hotel h = (Hotel) cb_hotel.getSelectedItem();
			
			}
		});

		
		bt_backupHotels.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				String timestamp = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
				File f1 = new File("src/adriaCode/BackupMasterDat" + timestamp);

				try (BufferedWriter writer = new BufferedWriter(new FileWriter(f1))) {
					for (Hotel hotel : HDAO.getAllHotels()) { 
		
						writer.write(hotel.hotelToCSV() + "\n");
					}
		
				} catch (Exception exc) {
					System.err.println(exc.getLocalizedMessage());
				}
			}

		});

		
		bt_sendEmail.addActionListener(new ActionListener() {
 
			@Override
			public void actionPerformed(ActionEvent e) {
				
			//	new EmailList ().setVisible(true);
				String mail = JOptionPane.showInputDialog("E-Mail Adress");
				if ((mail == null) || mail.equals(" ")) {
					JOptionPane.showMessageDialog(null, "Please enter a valid E-Mail Adress");
				}
				if (!(mail.contains("@"))) {
					JOptionPane.showMessageDialog(null, "The E-Mail adress is incorrect");
					return;
				}
				else { 
					
				}
				ArrayList<String> to = new ArrayList<String>();
			//	to.add("Michael.Deutsch@fh-vie.ac.at");
				to.add("fhb201482@fh-vie.ac.at");
				to.add("fhb201476@fh-vie.ac.at");
				to.add("fhb201479@fh-vie.ac.at");
				to.add("fhb191285@fh-vie.ac.at");
				to.add(mail); //Neue Mails hinzuf�gen 
				
				
				String user = "AdriaTourismGmbH@gmx.net";
				String pwd = "PiazziMa"; 				
				String host = "mail.gmx.net";
								
				Properties properties = System.getProperties();
				
				properties.put("mail.smtp.auth", "true");
				properties.put("mail.smtp.starttls.enable", "true");
				properties.put("mail.smtp.host", host);
				properties.put("mail.smtp.port", "587");
				JOptionPane.showMessageDialog(null, "Empf�nger hinzuf�gen");
				
				Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
					
					protected PasswordAuthentication getPasswordAuthentication() {
						
						return new PasswordAuthentication(user, pwd);
					}
				});
				
				try {
					MimeMessage message = new MimeMessage(session);

					message.setFrom(new InternetAddress(user));
					for (int i = 0; i <to.size(); i++) {
						message.addRecipient(Message.RecipientType.TO, new InternetAddress(to.get(i)));
					}
					
					String timestamp = new SimpleDateFormat("yyyyMMdd'.txt'").format(new Date());
					message.setSubject("Master Data " + timestamp);
					//message.setText(text);
					
					MimeMultipart multipart = new MimeMultipart();
					MimeBodyPart attachmentPart = new MimeBodyPart();
					
					File f2 = new File("src/E-Mail_" + timestamp);

					try (BufferedWriter writer = new BufferedWriter(new FileWriter(f2))) {
						for (Hotel hotel : HDAO.getAllHotels()) { 
			
							writer.write(hotel + "\n");
						}
			
					} catch (Exception exc) {
						System.err.println(exc.getLocalizedMessage());
					}
					attachmentPart.attachFile(f2);
					multipart.addBodyPart(attachmentPart);
					message.setContent(multipart); 
					
					Transport.send(message);
					
				} catch (MessagingException ex) {
					ex.printStackTrace();
				} catch (IOException ioex) {
					ioex.printStackTrace();
				}
			}

		});
	}


	public static DefaultComboBoxModel<Hotel> getModel() {
		return model;
	}


	public static int getIdCount() {
		return idCount;
	}


	public static void setIdCount(int idCount) {
		SearchHotel_ADRIA.idCount = idCount;
	}
}
