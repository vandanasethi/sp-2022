package functions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.microsoft.sqlserver.jdbc.StringUtils;

import connect.HDAO;
import connect.Hotel;
import overview.Category;

public class AddHotel extends JFrame {

	private String title = "Add Hotel";
	private Hotel hotel;
	JPanel insertAtt;

	private JTextField tf_hotelid;
	private JTextField tf_hotelcat;
	private JTextField tf_hotelname;
	private JTextField tf_hotelowner;
	private JTextField tf_contact;
	private JTextField tf_address;
	private JTextField tf_city;
	private JTextField tf_citycode;
	private JTextField tf_phone;
	private JTextField tf_nrofrooms;
	private JTextField tf_nrofbeds;
	private JButton bt_okay;
	
	public AddHotel() {
		
		setTitle(title);
		setSize(720, 200);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		
		insertAtt = new JPanel();
		hotel = new Hotel();
		hotel.setId(SearchHotel_ADRIA.getIdCount());
		
		tf_hotelid = new JTextField(5);
		tf_hotelcat = new JTextField(5);
		tf_hotelname = new JTextField(20);
		tf_hotelowner = new JTextField(15);
		tf_contact = new JTextField(15);
		tf_address = new JTextField(20);
		tf_city = new JTextField(10);
		tf_citycode = new JTextField(10);
		tf_phone = new JTextField(10);
		tf_nrofrooms = new JTextField(5);
		tf_nrofbeds = new JTextField(5);
		
		bt_okay = new JButton("Add this Hotel");
		
		insertAtt.add(new JLabel("ID:"));
		insertAtt.add(tf_hotelid);
		tf_hotelid.setEnabled(false);
		tf_hotelid.setText("" + hotel.getId());
		insertAtt.add(new JLabel("Category:"));
		insertAtt.add(tf_hotelcat);
		insertAtt.add(new JLabel("Name:"));
		insertAtt.add(tf_hotelname);
		insertAtt.add(new JLabel("Owner:"));
		insertAtt.add(tf_hotelowner);
		insertAtt.add(new JLabel("Contact:"));
		insertAtt.add(tf_contact);
		insertAtt.add(new JLabel("Address:"));
		insertAtt.add(tf_address);
		insertAtt.add(new JLabel("City:"));
		insertAtt.add(tf_city);
		insertAtt.add(new JLabel("Citycode:"));
		insertAtt.add(tf_citycode);
		insertAtt.add(new JLabel("Phone:"));
		insertAtt.add(tf_phone);
		insertAtt.add(new JLabel("Number Of Rooms:"));
		insertAtt.add(tf_nrofrooms);
		insertAtt.add(new JLabel("Number Of Beds:"));
		insertAtt.add(tf_nrofbeds);
		insertAtt.add(bt_okay);
		add(insertAtt);
		
		bt_okay.addActionListener(new ActionListener() { 
		
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String category = tf_hotelcat.getText();
                if (category.equals("*")) {
                    hotel.setCategory(Category.ONE);
                } else if (category.equals("**")) {
                    hotel.setCategory(Category.TWO);
                } else if (category.equals("***")) {
                    hotel.setCategory(Category.THREE);
                } else if (category.equals("****")) {
                    hotel.setCategory(Category.FOUR);
                } else if (category.equals("*****")) {
                    hotel.setCategory(Category.FIVE);
                } else {
                    JOptionPane.showMessageDialog(null, "'Category:' must have a value from * to *****");
                }
				
				if (tf_hotelname.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Name:' cannot be empty");
					return;
				} else {
					hotel.setName(tf_hotelname.getText());
				}
				
				if (tf_hotelowner.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Owner:' cannot be empty");
					return;
				} else {
					hotel.setOwner(tf_hotelowner.getText());
				}
				
				if (tf_contact.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Contact:' cannot be empty");
					return;
				} else {
					hotel.setContact(tf_contact.getText());
				}
				
				if (tf_address.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Address:' cannot be empty");
					return;
				} else {
					hotel.setAddress(tf_address.getText());
				}
				
				if (tf_city.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'City:' cannot be empty");
					return;
				} else {
					hotel.setCity(tf_city.getText());
				}
			
				if (tf_citycode.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Citycode:' cannot be empty");
					return;
				} else {
					hotel.setCitycode(tf_citycode.getText());
				}

				if (tf_phone.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "'Phone:' cannot be empty");
					return;
				} else {
					hotel.setPhone(tf_phone.getText());
				}
				
				if (StringUtils.isInteger(tf_nrofrooms.getText())) {
					hotel.setNumRooms(Integer.valueOf(tf_nrofrooms.getText()));
				} else {
					JOptionPane.showMessageDialog(null, "'Number Of Rooms:' must be a number");
					return;
				}
				if (StringUtils.isInteger(tf_nrofbeds.getText())) {
					hotel.setNumBeds(Integer.valueOf(tf_nrofbeds.getText()));
				} else {
					JOptionPane.showMessageDialog(null, "'Number Of Beds:' must be a number");
					return;
				}
				
				//System.out.println(hotel);
				SearchHotel_ADRIA.getModel().addElement(hotel);
				HDAO.persist(hotel);
				SearchHotel_ADRIA.setIdCount(SearchHotel_ADRIA.getIdCount() +1);
				setVisible(false);
	
			}
		});
	}

	
	
	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
}
