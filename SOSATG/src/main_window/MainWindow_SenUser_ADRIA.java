package main_window;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import overview.StatChartOverview_ADRIA;


public class MainWindow_SenUser_ADRIA extends JFrame implements ActionListener {
	
	private JButton bt_HoTa_Search;
	private JButton bt_StatChart_Ov;

	
	private String title = "ADRIA_TourismGmbH_SeniorUser_MainMenu";
	private JLabel logo;
	private Icon adria;
	
	public MainWindow_SenUser_ADRIA() {

		initComponents();
		descWindow();
		addToJFrame();
		addAction();
	}

	public void descWindow() {
		setTitle(title);
		setSize(1000, 480);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
		logo = new JLabel(adria);
	}

	public void addAction() {
		bt_HoTa_Search.addActionListener(this);
		bt_StatChart_Ov.addActionListener(this);
	}

	public void addToJFrame() { 
		add(logo, BorderLayout.PAGE_START);
		add(bt_HoTa_Search, BorderLayout.CENTER);
		add(bt_StatChart_Ov, BorderLayout.AFTER_LAST_LINE);
	}

	public void initComponents() {
		bt_HoTa_Search = new JButton("Hotel And Transaction Search");
		bt_StatChart_Ov = new JButton("Statistics And Charts Overview");
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == bt_HoTa_Search) {
			new functions.SearchHotel_ADRIA().setVisible(true);
		} else if (event.getSource() == bt_StatChart_Ov) {
			new StatChartOverview_ADRIA().setVisible(true);
	}

	}
}
