package main_window;

import java.awt.BorderLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;

public class MainWindow_HotelRep_ADRIA extends JFrame {

	private JLabel logo;
	private Icon adria;

	public MainWindow_HotelRep_ADRIA() {

		descWindow();
		addToJFrame();
	}

	public void descWindow() {
		setTitle("ADRIA_TourismGmbH_HotelRepresentative_MainMenu");
		setSize(1000, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
		logo = new JLabel(adria);
	}

	public void addToJFrame() {
		add(logo, BorderLayout.NORTH);		
		add(new JButton("Add Transaction"),BorderLayout.EAST);
		add(new JButton("View Transactions"),BorderLayout.CENTER);
		add(new JButton("Hotel Information"),BorderLayout.WEST);	
	}
}
