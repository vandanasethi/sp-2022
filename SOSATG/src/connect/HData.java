package connect;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import overview.Category;


public class HData {
	
	public static void main(String[] args) throws FileNotFoundException{
		File all = new File("./src/Connect/hotels.txt");
		Scanner scan = new Scanner(all);
		ArrayList<Hotel> allHotels = new ArrayList<>();

		while(scan.hasNext()) {
			String kette = scan.nextLine();
			String[] parts = kette.split(",");

			Hotel h;
			try {
				int id = Integer.parseInt(parts[0]);
				String category = parts[1];
				String hotelname = parts[2];
				String owner = parts[3];
				String contact = parts[4];
				String address = parts[5];
				String city = parts[6];
				String cityCode = parts[7];
				String phone = parts[8];
				int noRooms = Integer.parseInt(parts[9]);
				int noBeds = Integer.parseInt(parts[10]);
				
				Category catEnumerated = null;
                if (category.equals(Category.ONE.toString())) {
                    catEnumerated = Category.ONE;
                    
                } else if (category.equals(Category.TWO.toString())) {
                    catEnumerated = Category.TWO;
                    
                } else if (category.equals(Category.THREE.toString())) {
                    catEnumerated = Category.THREE;
                    
                } else if (category.equals(Category.FOUR.toString())) {
                    catEnumerated = Category.FOUR;
                
                } else if (category.equals(Category.FIVE.toString())) {
                    catEnumerated = Category.FIVE;
                }
                System.out.println(catEnumerated);
                
				h = new Hotel(id, catEnumerated, hotelname, owner, contact, address, city, cityCode, phone, noRooms, noBeds);
				allHotels.add(h);
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}

		System.out.println(allHotels.size());
		HDAO.persist2(allHotels);

	}
}
