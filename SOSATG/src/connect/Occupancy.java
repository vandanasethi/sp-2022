package connect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
public class Occupancy {
//	private static Map<Integer, Hotel> occupancyByID = new HashMap<>();
	private static ArrayList<Occupancy> everyOccupancy = new ArrayList<>();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ID;
	
	@NonNull
	private int hotelId;
	@NonNull
	private int rooms;
	@NonNull
	private int usedRooms;
	@NonNull
	private int beds;
	@NonNull
	private int usedBeds;
	@NonNull
	private int year;
	@NonNull
	private int month;
	

	public static ArrayList<Occupancy> everyOccupancy() {
		return everyOccupancy;
	}

	public static void setEveryOccupancy(ArrayList<Occupancy> everyOccupancy) {
		Occupancy.everyOccupancy = everyOccupancy;
	}

	public static Occupancy[] getAllOccupancies() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
