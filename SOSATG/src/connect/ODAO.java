package connect;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import connect.Occupancy;

public class ODAO {
	public static void main(String[] args) {

	}

	private static Occupancy search(int o_id) {
		Session session = getOpenSession();

		Occupancy o = (Occupancy) session.get(Occupancy.class, o_id);
		return o;
	}
	public static int update(Occupancy o) {
		Session session = getOpenSession();

		try {
			session.beginTransaction();
			session.update(o);
			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return o.getHotelId();
	}

	private static int persist(Occupancy o) {
		Session session = getOpenSession();

		try {
			session.beginTransaction();
			session.save(o);
			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return o.getID();

	}

	public static void persist2(List<Occupancy> all) {
		Session session = getOpenSession();

		try {

			// session.save(o)

			session.beginTransaction();
			for (Occupancy o : all) {
				session.save(o);
			}

			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		System.out.println("Done: " + all.size());
	}
	public static List<Occupancy> getAllOccupancies() {
		Session session = getOpenSession();
		List<Occupancy> hotels = new ArrayList<Occupancy>();
		try {
			session.beginTransaction();
			hotels = session.createQuery("FROM Occupancy", Occupancy.class).list();  

			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		session.close();
		return getAllOccupancies();
	}

	private static Session getOpenSession() {
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Occupancy.class)
				.buildSessionFactory();
		return factory.openSession();
	}
		
	}
