package connect;

	import java.io.File;
	import java.io.FileNotFoundException;
	import java.util.ArrayList;
	import java.util.List;
	import java.util.Scanner;
	
	public class OData {
		
		
		public static void main(String[] args) throws FileNotFoundException {
			File f = new File("./src/Connect/occupancies.txt");
			Scanner s = new Scanner(f);
			List<Occupancy> all = new ArrayList<>();

			while (s.hasNext()) {
				String line = s.nextLine();
				String[] parts = line.split(",");

				
				try {
					int hotelId = Integer.parseInt(parts[0]);
					int rooms = Integer.parseInt(parts[1]);
					int usedRooms = Integer.parseInt(parts[2]);
					int beds = Integer.parseInt(parts[3]);
					int usedBeds = Integer.parseInt(parts[4]);
					int year = Integer.parseInt(parts[5]);
					int month = Integer.parseInt(parts[6]);

					
					Occupancy o = new Occupancy (hotelId, rooms, usedRooms, beds, usedBeds, year, month);
					all.add(o);
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			
			}
			
			System.out.println(all.size());
			ODAO.persist2(all);

			
			
		}


}
