package connect;

import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;

import lombok.AllArgsConstructor;
	import lombok.Data;
	import lombok.NoArgsConstructor;
	import lombok.NonNull;
	import lombok.RequiredArgsConstructor;
	import lombok.ToString;
import overview.Category;

	@Data
	@NoArgsConstructor
	@RequiredArgsConstructor
	@Entity
	@AllArgsConstructor
	@ToString
	public class Hotel {


		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int id;
		
		@NonNull
		private Category category;
		@NonNull
		private String name;
		@NonNull
		private String owner;
		@NonNull
		private String contact;
		@NonNull
		private String address;
		@NonNull
		private String city;
		@NonNull
		private String citycode;
		@NonNull
		private String phone;
		@NonNull
		private int numRooms;
		@NonNull
		private int numBeds;
		
		@Override
        public String toString() {
            return name;
        }

        public String hotelToCSV() {

            return this.id + "," +
            this.category + "," +
            this.name + "," +
            this.owner + "," +
            this.contact + "," +
            this.address + "," +
            this.city + "," +
            this.citycode + "," +
            this.phone + "," +
            this.numRooms + "," +
            this.numBeds;
        }
		

}
