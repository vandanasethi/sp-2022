package overview;

public enum Category {
	ONE("*"), TWO("**"), THREE("***"), FOUR("****"), FIVE("*****");
	
	private String stars;
	
	private Category(String stars) {
		this.stars = stars;
	}
	
	public String toString() {
		return stars;
	}

	public static Category fromString(String text) {
        for (Category c : Category.values()) {
            if (c.stars.equalsIgnoreCase(text)) {
                return c;
            }
        }
        return null;
    }
	
	}
