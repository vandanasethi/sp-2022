package overview;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import connect.HDAO;
import connect.Hotel;


public class SummaryMasterDat_ADRIA extends JFrame {

	JPanel top_panel;
	private JLabel logo;
	private Icon adria;	
	
	ArrayList<HotelPerCat> hotelsPerCategory = new ArrayList<HotelPerCat>();

	public SummaryMasterDat_ADRIA() {

		setSize(950, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		adria = new ImageIcon(getClass().getResource("2022-ATP-Logo.jpg"));
		logo = new JLabel(adria);
		add(logo, BorderLayout.NORTH);

		String[] columns = {"Categories", "Establishments", "Rooms", "Beds"};		
		DefaultTableModel model = new DefaultTableModel();
		
		hotelsPerCategory.add(new HotelPerCat(Category.ONE)); 
		hotelsPerCategory.add(new HotelPerCat(Category.TWO));
		hotelsPerCategory.add(new HotelPerCat(Category.THREE));
		hotelsPerCategory.add(new HotelPerCat(Category.FOUR));
		hotelsPerCategory.add(new HotelPerCat(Category.FIVE));
		
		for (HotelPerCat hotel : hotelsPerCategory) {
			CountCategories((List<Hotel>)HDAO.getAllHotels(), hotel);
		}
		
		for (String column : columns) {
			model.addColumn(column);
		}
		
		model.addRow(new Object[] {"Categories" , "Establishments", "Rooms", "Beds"});
		for (HotelPerCat hotelPerCategory : hotelsPerCategory) {
			model.addRow(new Object[] {hotelPerCategory.getCategory(), hotelPerCategory.getNoOfEstablishments(), hotelPerCategory.getNoOfRooms(), hotelPerCategory.getNoOfBeds()});
		}

		JTable table = new JTable(model);
		add(table);
		
		setVisible(true);

	}
	
	public HotelPerCat CountCategories(List<Hotel> hotelInp, HotelPerCat categoryInp) {
				
		for(Hotel hotel : hotelInp) {
			if(hotel.getCategory()== categoryInp.getCategory()) {
				categoryInp.setNoOfEstablishments(categoryInp.getNoOfEstablishments() +1);
				categoryInp.setNoOfRooms(categoryInp.getNoOfRooms() + hotel.getNumRooms());
				categoryInp.setNoOfBeds(categoryInp.getNoOfBeds() + hotel.getNumBeds());
			}
		}
		return categoryInp;
	}

}
